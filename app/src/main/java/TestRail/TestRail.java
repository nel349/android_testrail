package TestRail;


import android.util.Log;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by norman.lopez on 3/5/15.
 */
public class TestRail {
    Logger log = Logger.getLogger("Testrail");
    APIClient client = null;

    public TestRail(String username, String password){
        client = new APIClient("https://mutualmobile.testrail.com/");
        client.setUser(username);
        client.setPassword(password);
    }


    public String[] get_projects() throws APIException, IOException {

        JSONArray b = (JSONArray) client.sendGet("get_projects/");
        String[] projects = new String[b.size()];
        for(int i =0; i < b.size(); ++i){
            JSONObject x = (JSONObject)b.get(i);
            String project = (String) x.get("name");
            projects[i] =project;
            Log.i("projects", "project: " + project);
        }
        return projects;
    }
}
