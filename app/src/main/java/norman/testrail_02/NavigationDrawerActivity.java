/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package norman.testrail_02;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.List;
import java.util.concurrent.ExecutionException;

import TestRail.TestRail;
import slidingbar.SlidingTabLayout;
import testcase_list.CustomAdapter;


/**
 * This example illustrates a common usage of the DrawerLayout widget
 * in the Android support library.
 * <p/>
 * <p>When a navigation (left) drawer is present, the host activity should detect presses of
 * the action bar's Up affordance as a signal to open and close the navigation drawer. The
 * ActionBarDrawerToggle facilitates this behavior.
 * Items within the drawer should fall into one of two categories:</p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic policies as
 * list or tab navigation in that a view switch does not create navigation history.
 * This pattern should only be used at the root activity of a task, leaving some form
 * of Up navigation active for activities further down the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an alternate
 * parent for Up navigation. This allows a user to jump across an app's navigation
 * hierarchy at will. The application should treat this as it treats Up navigation from
 * a different task, replacing the current task stack using TaskStackBuilder or similar.
 * This is the only form of navigation drawer that should be used outside of the root
 * activity of a task.</li>
 * </ul>
 * <p/>
 * <p>Right side drawers should be used for actions, not navigation. This follows the pattern
 * established by the Action Bar that navigation should be to the left and actions to the right.
 * An action should be an operation performed on the current contents of the window,
 * for example enabling or disabling a data overlay on top of the current content.</p>
 */
public class NavigationDrawerActivity extends FragmentActivity implements PlanetAdapter.OnItemClickListener {
    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    public static String[] mPlanetTitles;

    private  String username;
    private String password;
    private TestRail tr;
    public List<String> projects;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        username = getIntent().getStringExtra("email");
        password = getIntent().getStringExtra("password");
        TestRailtask project_task = new TestRailtask(username, password);
        try {
            mPlanetTitles  = project_task.execute().get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


//        Log.i("testA", mPlanetTitles[0]);
//        Log.i("testA", mPlanetTitles[1]);
        mTitle = mDrawerTitle = getTitle();
//        mPlanetTitles = getResources().getStringArray(R.array.planets_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (RecyclerView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // improve performance by indicating the list if fixed size.
        mDrawerList.setHasFixedSize(true);
        mDrawerList.setLayoutManager(new LinearLayoutManager(this));

        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new PlanetAdapter(mPlanetTitles, this));
        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_launcher,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
                // catch event that there's no activity to handle intent
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listener for RecyclerView in the navigation drawer */
    @Override
    public void onClick(View view, int position) {
        selectItem(position);
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = SlidingTabsBasicFragment.newInstance(position);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.sample_content_fragment, fragment);
        ft.commit();

        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
//        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Fragment that appears in the "content_frame", shows a planet
     */
    public static class SlidingTabsBasicFragment extends Fragment {
        public static final String ARG_PLANET_NUMBER = "planet_number";

        public SlidingTabsBasicFragment() {
            // Empty constructor required for fragment subclasses
        }

        public static Fragment newInstance(int position) {
            Fragment fragment = new SlidingTabsBasicFragment();
            Bundle args = new Bundle();
            args.putInt(SlidingTabsBasicFragment.ARG_PLANET_NUMBER, position);
            fragment.setArguments(args);
            return fragment;
        }

        /**
         * A custom {@link android.support.v4.view.ViewPager} title strip which looks much like Tabs present in Android v4.0 and
         * above, but is designed to give continuous feedback to the user when scrolling.
         */
        private SlidingTabLayout mSlidingTabLayout;

        /**
         * A {@link android.support.v4.view.ViewPager} which will be used in conjunction with the {@link SlidingTabLayout} above.
         */
        private ViewPager mViewPager;

        /**
         * Inflates the {@link android.view.View} which will be displayed by this {@link android.support.v4.app.Fragment}, from the app's
         * resources.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_sample, container, false);
        }

        // BEGIN_INCLUDE (fragment_onviewcreated)
        /**
         * This is called after the {@link #onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)} has finished.
         * Here we can pick out the {@link android.view.View}s we need to configure from the content view.
         *
         * We set the {@link android.support.v4.view.ViewPager}'s adapter to be an instance of {@link SamplePagerAdapter}. The
         * {@link SlidingTabLayout} is then given the {@link android.support.v4.view.ViewPager} so that it can populate itself.
         *
         * @param view View created in {@link #onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)}
         */
        @Override
        public void onViewCreated(View view, Bundle savedInstanceState) {
            // BEGIN_INCLUDE (setup_viewpager)
            // Get the ViewPager and set it's PagerAdapter so that it can display items
            mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
            mViewPager.setAdapter(new SamplePagerAdapter());
            // END_INCLUDE (setup_viewpager)

            // BEGIN_INCLUDE (setup_slidingtablayout)
            // Give the SlidingTabLayout the ViewPager, this must be done AFTER the ViewPager has had
            // it's PagerAdapter set.
            mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
            mSlidingTabLayout.setViewPager(mViewPager);
            // END_INCLUDE (setup_slidingtablayout)
        }
        // END_INCLUDE (fragment_onviewcreated)

        private enum LayoutManagerType {
            GRID_LAYOUT_MANAGER,
            LINEAR_LAYOUT_MANAGER
        }
        /**
         * The {@link android.support.v4.view.PagerAdapter} used to display pages in this sample.
         * The individual pages are simple and just display two lines of text. The important section of
         * this class is the {@link #getPageTitle(int)} method which controls what is displayed in the
         * {@link SlidingTabLayout}.
         */
        class SamplePagerAdapter extends PagerAdapter {

            /**
             * @return the number of pages to display
             */
            @Override
            public int getCount() {
                return 4;
            }

            /**
             * @return true if the value returned from {@link #instantiateItem(android.view.ViewGroup, int)} is the
             * same object as the {@link android.view.View} added to the {@link android.support.v4.view.ViewPager}.
             */
            @Override
            public boolean isViewFromObject(View view, Object o) {
                return o == view;
            }

            // BEGIN_INCLUDE (pageradapter_getpagetitle)
            /**
             * Return the title of the item at {@code position}. This is important as what this method
             * returns is what is displayed in the {@link SlidingTabLayout}.
             * <p>
             * Here we construct one using the position value, but for real application the title should
             * refer to the item's contents.
             */
            @Override
            public CharSequence getPageTitle(int position) {
                return "Item " + (position + 1);
            }
            // END_INCLUDE (pageradapter_getpagetitle)


            private static final String TAG = "RecyclerViewFragment";
            private static final String KEY_LAYOUT_MANAGER = "layoutManager";
            private static final int SPAN_COUNT = 2;
            private static final int DATASET_COUNT = 60;



            protected LayoutManagerType mCurrentLayoutManagerType;

            protected RadioButton mLinearLayoutRadioButton;
            protected RadioButton mGridLayoutRadioButton;

            protected RecyclerView mRecyclerView;
            protected CustomAdapter mAdapter;
            protected RecyclerView.LayoutManager mLayoutManager;
            protected String[] mDataset;
            /**
             * Instantiate the {@link android.view.View} which should be displayed at {@code position}. Here we
             * inflate a layout from the apps resources and then change the text view to signify the position.
             */
            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                initDataset();

                // Inflate a new layout from our resources
                View rootView = getActivity().getLayoutInflater().inflate(R.layout.recycler_view_frag,
                        container, false);
                // Add the newly created View to the ViewPager
//                LinearLayout ll = (LinearLayout)view.findViewById(R.id.tab_content);
//                ll.addView(tv1);
//                ll.addView(tv2);
//                ll.addView(tv3);

                mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

                // LinearLayoutManager is used here, this will layout the elements in a similar fashion
                // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
                // elements are laid out.
                mLayoutManager = new LinearLayoutManager(getActivity());

                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;

                setRecyclerViewLayoutManager(mCurrentLayoutManagerType);

                mAdapter = new CustomAdapter(mDataset);
                // Set CustomAdapter as the adapter for RecyclerView.
                mRecyclerView.setAdapter(mAdapter);
                // END_INCLUDE(initializeRecyclerView)

                mLinearLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.linear_layout_rb);
                mLinearLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRecyclerViewLayoutManager(LayoutManagerType.LINEAR_LAYOUT_MANAGER);
                    }
                });

                mGridLayoutRadioButton = (RadioButton) rootView.findViewById(R.id.grid_layout_rb);
                mGridLayoutRadioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRecyclerViewLayoutManager(LayoutManagerType.GRID_LAYOUT_MANAGER);
                    }
                });


                container.addView(rootView);

                // Retrieve a TextView from the inflated View, and update it's text
//                TextView title = (TextView) view.findViewById(R.id.item_title);
//                title.setText(String.valueOf(position + 1));

//            Log.i(LOG_TAG, "instantiateItem() [position: " + position + "]");

                // Return the View
                return rootView;
            }

            /**
             * Destroy the item from the {@link android.support.v4.view.ViewPager}. In our case this is simply removing the
             * {@link android.view.View}.
             */
            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
//            Log.i(LOG_TAG, "destroyItem() [position: " + position + "]");
            }


            /**
             * Set RecyclerView's LayoutManager to the one given.
             *
             * @param layoutManagerType Type of layout manager to switch to.
             */
            public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
                int scrollPosition = 0;

                // If a layout manager has already been set, get current scroll position.
                if (mRecyclerView.getLayoutManager() != null) {
                    scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                            .findFirstCompletelyVisibleItemPosition();
                }

                switch (layoutManagerType) {
                    case GRID_LAYOUT_MANAGER:
                        mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                        mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                        break;
                    case LINEAR_LAYOUT_MANAGER:
                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                        break;
                    default:
                        mLayoutManager = new LinearLayoutManager(getActivity());
                        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                }

                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.scrollToPosition(scrollPosition);
            }


            /**
             * Generates Strings for RecyclerView's adapter. This data would usually come
             * from a local content provider or remote server.
             */
            private void initDataset() {
                mDataset = new String[DATASET_COUNT];
                for (int i = 0; i < DATASET_COUNT; i++) {
                    mDataset[i] = "This is element #" + i;
                }
            }

        }



    }




    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class TestRailtask extends AsyncTask<String, Void, String[]> {

        private final String mEmail;
        private final String mPassword;
        TestRail tr = null;
        TestRailtask(String email, String password) {
            mEmail = email;
            mPassword = password;
            Log.i("credentials", "email: " + mEmail);
            Log.i("credentials", "password: "+ mPassword);
        }

        @Override
        protected String[] doInBackground(String... params) {
            // TODO: attempt authentication against a network service.
            String[] result;
            try {
                tr = new TestRail(mEmail, mPassword);
                Log.i("authenticating", "in side drawer");
                Thread.sleep(1000);
                result = tr.get_projects();


            } catch (Exception e) {
                return null;
            }
            return result;
        }

//        @Override
//        protected void onPostExecute(final List<String> success) {
//
//            if(success != null) {
//                Log.i("post_execute", "Post Executed after get projects");
//                mPlanetTitles = projects.toArray(mPlanetTitles);
//                Log.i("post_execute", mPlanetTitles[0]);
//                Log.i("post_execute", mPlanetTitles[1]);
//                Log.i("post_execute", mPlanetTitles[2]);
//
//
//            }
//
//        }
    }
}
